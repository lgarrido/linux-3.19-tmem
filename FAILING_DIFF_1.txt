diff --git a/drivers/xen/tmem.c b/drivers/xen/tmem.c
index 83b5c53..3d5a731 100644
--- a/drivers/xen/tmem.c
+++ b/drivers/xen/tmem.c
@@ -22,6 +22,7 @@
 #include <asm/xen/hypervisor.h>
 #include <xen/tmem.h>
 
+
 #ifndef CONFIG_XEN_TMEM_MODULE
 bool __read_mostly tmem_enabled = false;
 
@@ -33,6 +34,7 @@ static int __init enable_tmem(char *s)
 __setup("tmem", enable_tmem);
 #endif
 
+
 #ifdef CONFIG_CLEANCACHE
 static bool cleancache __read_mostly = true;
 module_param(cleancache, bool, S_IRUGO);
@@ -126,6 +128,8 @@ static int xen_tmem_new_pool(struct tmem_pool_uuid uuid,
 	return rc;
 }
 
+
+
 /* xen generic tmem ops */
 
 static int xen_tmem_put_page(u32 pool_id, struct tmem_oid oid,
@@ -199,6 +203,12 @@ static int tmem_cleancache_get_page(int pool, struct cleancache_filekey key,
 	ret = xen_tmem_get_page((u32)pool, oid, ind, pfn);
 	if (ret == 1)
 		return 0;
+        else if (ret == 2)
+        {
+            printk("Cleancache should not be being used!\n");
+            BUG_ON(ret == 2); // remote pages for cleancache NOT implemented yet
+            return 0;
+        }
 	else
 		return -1;
 }
@@ -325,6 +335,10 @@ static int tmem_frontswap_load(unsigned type, pgoff_t offset,
 	/* translate Xen tmem return values to linux semantics */
 	if (ret == 1)
 		return 0;
+        else if (ret == 2)
+        {
+            return 2;
+        }
 	else
 		return -1;
 }
@@ -374,10 +388,22 @@ static struct frontswap_ops tmem_frontswap_ops = {
 };
 #endif
 
+
+
+
 static int xen_tmem_init(void)
 {
+        int ret;
 	if (!xen_domain())
 		return 0;
+
+        if (xen_initial_domain())
+        {
+            pr_info("tmem: remote access server - disable frontswap and cleancache (for now)\n");
+            frontswap = 0;
+            cleancache = 0;
+        }
+
 #ifdef CONFIG_FRONTSWAP
 	if (tmem_enabled && frontswap) {
 		char *s = "";
@@ -387,7 +413,7 @@ static int xen_tmem_init(void)
 		old_ops = frontswap_register_ops(&tmem_frontswap_ops);
 		if (IS_ERR(old_ops) || old_ops) {
 			if (IS_ERR(old_ops))
-				return PTR_ERR(old_ops);
+                            return PTR_ERR(old_ops);
 			s = " (WARNING: frontswap_ops overridden)";
 		}
 		pr_info("frontswap enabled, RAM provided by Xen Transcendent Memory%s\n",
@@ -395,6 +421,8 @@ static int xen_tmem_init(void)
 	}
 #endif
 #ifdef CONFIG_CLEANCACHE
+        cleancache = 0; // PMC: only handling frontswap for now
+        printk("hello\n");
 	BUG_ON(sizeof(struct cleancache_filekey) != sizeof(struct tmem_oid));
 	if (tmem_enabled && cleancache) {
 		char *s = "";
@@ -417,6 +445,7 @@ static int xen_tmem_init(void)
 	}
 	xen_selfballoon_init(selfballooning, selfshrinking);
 #endif
+
 	return 0;
 }
 
diff --git a/include/xen/interface/xen.h b/include/xen/interface/xen.h
index f68719f..c8fcc67 100644
--- a/include/xen/interface/xen.h
+++ b/include/xen/interface/xen.h
@@ -739,6 +739,12 @@ struct tmem_op {
 			uint32_t len;
 			GUEST_HANDLE(void) gmfn; /* guest machine page frame */
 		} gen;
+                struct {
+                        uint64_t page_ptr_tmp;
+                        uint32_t max_put_pages;
+                        uint32_t max_get_pages;
+                        uint64_t got_block_id;
+                } remote;
 	} u;
 };
 
diff --git a/mm/frontswap.c b/mm/frontswap.c
index 8d82809..ede6e18 100644
--- a/mm/frontswap.c
+++ b/mm/frontswap.c
@@ -276,8 +276,11 @@ int __frontswap_load(struct page *page)
 	 */
 	if (__frontswap_test(sis, offset))
 		ret = frontswap_ops->load(type, offset, page);
-	if (ret == 0) {
+	if (ret == 0 || ret == 2) {
 		inc_frontswap_loads();
+        }
+        if (ret == 0) {
+            // not sure about this, but shouldn't be relevant now
 		if (frontswap_tmem_exclusive_gets_enabled) {
 			SetPageDirty(page);
 			__frontswap_clear(sis, offset);
diff --git a/mm/page_io.c b/mm/page_io.c
index 955db8b..cc0f562 100644
--- a/mm/page_io.c
+++ b/mm/page_io.c
@@ -341,11 +341,18 @@ int swap_readpage(struct page *page)
 
 	VM_BUG_ON_PAGE(!PageLocked(page), page);
 	VM_BUG_ON_PAGE(PageUptodate(page), page);
-	if (frontswap_load(page) == 0) {
+	ret = frontswap_load(page);
+        if (ret == 0) {
+            // succeeded and page available now
 		SetPageUptodate(page);
 		unlock_page(page);
 		goto out;
-	}
+	} else if (ret == 2) {
+            // succeeded, but remote page => do not unlock yet
+            ret = 0;
+            goto out;
+        }
+
 
 	if (sis->flags & SWP_FILE) {
 		struct file *swap_file = sis->swap_file;
