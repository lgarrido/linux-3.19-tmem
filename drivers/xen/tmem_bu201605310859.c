/*
 * Xen implementation for transcendent memory (tmem)
 *
 * Copyright (C) 2009-2011 Oracle Corp.  All rights reserved.
 * Author: Dan Magenheimer
 */

#define pr_fmt(fmt) "xen:" KBUILD_MODNAME ": " fmt

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/pagemap.h>
#include <linux/cleancache.h>
#include <linux/frontswap.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/circ_buf.h>
#include <linux/slab.h>

/* For socket communication */
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>
/* For socket communication */

#include <xen/xen.h>
#include <xen/events.h>
#include <xen/interface/xen.h>
#include <asm/xen/hypercall.h>
#include <asm/xen/page.h>
#include <asm/xen/hypervisor.h>
#include <xen/tmem.h>

DECLARE_WAIT_QUEUE_HEAD(procwait);  // for blocking reads on /proc/tmem_remote
static volatile int procready = 0;
/**/				// ++ platero, start
DECLARE_WAIT_QUEUE_HEAD(procwait_tmem);  // for blocking reads on /proc/tmem_remote
static volatile int procready_tmem = 0;

DECLARE_WAIT_QUEUE_HEAD(tuopwait);
static volatile int tuopready = 1;
/**/				// ++ platero, end

#ifndef CONFIG_XEN_TMEM_MODULE
bool __read_mostly tmem_enabled = false;


static int __init enable_tmem(char *s)
{
	tmem_enabled = true;
	return 1;
}
__setup("tmem", enable_tmem);
#endif

static DEFINE_SPINLOCK(remote_spinlock);
/**/					// ++ platero, start
static DEFINE_SPINLOCK(remote_tstats_spinlock);
/**/					// ++ platero, end

static void *status_page = NULL;

struct temp_page_t {
    struct list_head free_temp_pages;
    void *page_ptr;
};

struct waiting_page_t {
    struct list_head waiting_pages;
    struct page *page;
    int pool;
    unsigned type;
    u32 ind;
};

#define NUM_TEMP_PAGES 32
struct temp_page_t temp_pages[NUM_TEMP_PAGES];
static LIST_HEAD(free_temp_pages_list);
static LIST_HEAD(get_waiting_pages_list);

#ifdef CONFIG_CLEANCACHE
static bool cleancache __read_mostly = true;
module_param(cleancache, bool, S_IRUGO);
static bool selfballooning __read_mostly = true;
module_param(selfballooning, bool, S_IRUGO);
#endif /* CONFIG_CLEANCACHE */

#ifdef CONFIG_FRONTSWAP
static bool frontswap __read_mostly = true;
module_param(frontswap, bool, S_IRUGO);
#else /* CONFIG_FRONTSWAP */
#define frontswap (0)
#endif /* CONFIG_FRONTSWAP */

#ifdef CONFIG_XEN_SELFBALLOONING
static bool selfshrinking __read_mostly = true;
module_param(selfshrinking, bool, S_IRUGO);
#endif /* CONFIG_XEN_SELFBALLOONING */

#define TMEM_CONTROL               0
#define TMEM_NEW_POOL              1
#define TMEM_DESTROY_POOL          2
#define TMEM_NEW_PAGE              3
#define TMEM_PUT_PAGE              4
#define TMEM_GET_PAGE              5
#define TMEM_FLUSH_PAGE            6
#define TMEM_FLUSH_OBJECT          7
#define TMEM_READ                  8
#define TMEM_WRITE                 9
#define TMEM_XCHG                 10
#define TMEM_SERVER_REMOTE_STATUS 50
#define TMEM_CLIENT_REMOTE_STATUS 51
#define TMEM_GOT_PAGE             52

/* ++ platero. Reading tmem stats */			
#define TMEM_READSTATS            53
#define TMEM_RSVPG            	  54
#define TMEM_SET_TOTAL_LIST       55
#define TMEM_SET_LOCAL_LIST       56
#define TMEM_SET_REMOTE_LIST      57
#define TMEM_SET_HYP_NODE_INF	  58

#define TMEM_MOVE_TOTAL_TO_LOCAL   59
#define TMEM_MOVE_LOCAL_TO_TOTAL   62
#define TMEM_MOVE_TOTAL_TO_REMOTE  63
#define TMEM_MOVE_REMOTE_TO_TOTAL  64

#define TMEM_REQUEST_FROM_REMOTE  60
#define TMEM_RECEIVED_FROM_REMOTE 61
#define TMEM_RELINQUISH_REMOTE	  65
#define TMEM_RELACK_REMOTE	  66
#define TMEM_RECLAIM_REMOTE	  67
#define TMEM_RECLACK_REMOTE	  68
/* ++ platero */

/* Bits for HYPERVISOR_tmem_op(TMEM_NEW_POOL) */
#define TMEM_POOL_PERSIST          1
#define TMEM_POOL_SHARED           2
#define TMEM_POOL_PAGESIZE_SHIFT   4
#define TMEM_VERSION_SHIFT        24


/* pending operations from the hypervisor, not yet given to user process
 */
#define MAX_PENDING_OPS 32 /* should be power of two */


// Communication commands in /proc/tmem_remote file
#define REMOTE_OP_NONE     0xa450000
#define REMOTE_OP_PUT      0xa450001
#define REMOTE_OP_GET      0xa450002
#define REMOTE_OP_DEL      0xa450003

/**/				// ++ platero, start
#define REMOTE_TMEM_STATS  0xa45000A
/**/				// ++ platero, end

// Communication replies
#define REMOTE_OP_GETOK    0xa450006
#define REMOTE_OP_GETFAIL  0xa450007
// #define REMOTE_OP_PUTOK    0xa450004
// #define REMOTE_OP_PUTFAIL  0xa450005
/**/				// ++ platero, start
#define REMOTE_OP_PID		0xa46000B
#define REMOTE_OP_PUT_KERNACK	0xa46000C
#define REMOTE_OP_GET_KERNACK	0xa46000D

#define MM_VOID		0xB0000
#define MM_TEST		0xB0001
#define MM_RSVPAG	0xB0002
#define MM_RELPAG	0xB0003

#define MM_SETUP_TFAL	0xB0005		// Setup the local free address list.
#define MM_SETUP_LFAL	0xB0006		// Setup the local free address list.
#define MM_SETUP_RFAL	0xB0007		// Setup the local free address list.

#define MM_SETUP_HNINF	0xB0008
#define MM_SEND_STATS	0xB0009

#define MM_TFAL_TO_LFAL 0xB000A		// Move from the total list to the local list.
#define MM_LFAL_TO_TFAL 0xB000D
#define MM_TFAL_TO_RFAL 0xB000E
#define MM_RFAL_TO_TFAL 0xB000F

#define MM_REQ_MCAP	0xB000B
#define MM_RCV_MCAP	0xB000C
#define MM_RELINQ_MCAP	0xB0010
#define MM_RELACK_MCAP	0xB0011
#define MM_RECLAIM_MCAP	0xB0013
#define MM_RECLACK_MCAP 0xB0012
/**/				// ++ platero, end

// Command sent to user process
struct remote_op
{
    uint32_t cmd;
    uint64_t block_id;
};

// Pending command to send to user process
struct pending_op_t {
    struct remote_op op;
    struct temp_page_t *temp_page_if_put;
};

/**/				// ++ platero, start
/*struct pending_tmop_t {
    struct remote_op op;
    struct tmop_cnt *temp_page_if_put;
};*/
/**/				// ++ platero, end

// Circular buffer for all the pending commands
struct pending_ops_t {
    struct pending_op_t ops[MAX_PENDING_OPS];
    int head;
    int tail;
};

/**/				// ++ platero, start
/*struct pending_tmops_t {
    struct pending_tmop_t ops[MAX_PENDING_OPS];
    int head;
    int tail;
};*/
/**/				// ++ platero, end

struct pending_ops_t pending_ops;
/**/				// ++ platero
//struct pending_tmops_t pending_tmops;

int init_setup=0;
struct sock *nl_sk = NULL;
struct sock *nl_tusk=NULL;
uint32_t mm_pid;
uint32_t tu_pid;
uint32_t trans_count=0;
uint32_t put_count = 0;
/**/				// ++ platero

// Reply from user process
struct remote_reply {
    struct remote_op op;
    char buf[4096];
};

struct tmem_pool_uuid {
	u64 uuid_lo;
	u64 uuid_hi;
};

struct tmem_oid {
	u64 oid[3];
};

#define TMEM_POOL_PRIVATE_UUID	{ 0, 0 }

/* flags for tmem_ops.new_pool */
#define TMEM_POOL_PERSIST          1
#define TMEM_POOL_SHARED           2

/**/				// ++ platero, start
int nl_send_msg(struct tmop_cnt *opcnt, uint32_t msg_size) {
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;

    skb_out = nlmsg_new(msg_size, 0);
    if (!skb_out) {
	printk(KERN_ERR "Failed to allocate new skb\n");
	return -10;
    }

    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_size, 0);
    NETLINK_CB(skb_out).dst_group = 0;
    memcpy( NLMSG_DATA(nlh), opcnt, msg_size);

    res = nlmsg_unicast(nl_sk, skb_out, mm_pid);
    //if (res < 0) {
	//printk(KERN_INFO "Error while sending bak to use\n");
    //}
    return res;
}

int nl_resp_mmhyp_cmd(struct mm_hyp_cmd *tcmd, uint32_t msg_size) {
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;

    skb_out = nlmsg_new(msg_size, 0);
    if (!skb_out) {
	printk(KERN_ERR "func %s, note: Failed to allocate new skb\n", __func__);
	return -10;
    }

    printk("\ttcmd->m.pglst_mng.len=%lu\n", (long unsigned int)tcmd->m.pglst_mng.len);

    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_size, 0);
    NETLINK_CB(skb_out).dst_group = 0;
    memcpy( NLMSG_DATA(nlh), tcmd, msg_size);

    res = nlmsg_unicast(nl_sk, skb_out, mm_pid);
    if (res < 0) {
	printk(KERN_INFO "func %s, note: Error while sending bak to use, res=%i\n", 
			__func__, res);
    }
    return res;
}

static int print_first_op_err=0;
int32_t nl_tuop_cmd(struct remote_op *op, uint32_t msg_size) {
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;

    //do {
	nlh = NULL;
	skb_out = NULL;

	do {
	    skb_out = nlmsg_new(msg_size, 0);
	    if (!skb_out) {
		printk(KERN_ERR "func %s, note: Failed to allocate new skb\n", __func__);
		//return -10;
	    }
	} while (skb_out == NULL);

	do {
	    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_size, 0);
	    if (nlh == NULL) {
		printk("func %s, note: nlh is NULL\n", __func__);
	    }
	} while (nlh == NULL);
	NETLINK_CB(skb_out).dst_group = 0;
	memcpy( NLMSG_DATA(nlh), op, msg_size);
    
	if (op->cmd == REMOTE_OP_PUT) {
	    put_count = put_count + 1;
	}

    //printk("\tfunc %s, note: op->cmd=%i, op->block_id=%lu, put_count=%i\n", 
	//	__func__, op->cmd, (long unsigned int)op->block_id, put_count);

//nl_tuop_cmd_unicast_retry:
	res = nlmsg_unicast(nl_tusk, skb_out, tu_pid);
	if (res == -11 && print_first_op_err==0) {
	    printk(KERN_INFO "func %s, note: Error while sending op to use, res=%i,put_count=%i\n", 
		__func__, res, put_count);
	    print_first_op_err = 0;
	    msleep(1000);
	    //res = nlmsg_unicast(nl_tusk, skb_out, tu_pid);
	    //goto nl_tuop_cmd_unicast_retry;
	}
	//nlmsg_free(skb_out);
    //} while (res < 0);
    return res;
}

static int print_first_buff_err=0;
int32_t nl_tuopbf_cmd(void *buff, uint32_t msg_size) {
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    int res;

    //do {
	nlh = NULL;
	skb_out = NULL;

	do {
	    skb_out = nlmsg_new(msg_size, 0);
	    if (!skb_out) {
		printk(KERN_ERR "func %s, note: Failed to allocate new skb\n", __func__);
		//return -10;
	    }
	} while (skb_out == NULL);

	do {
	    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_size, 0);
	    if (nlh == NULL) {
		printk("func %s, note: nlh is NULL\n", __func__);
	    }
	} while (nlh == NULL);
	NETLINK_CB(skb_out).dst_group = 0;
	memcpy( NLMSG_DATA(nlh), buff, msg_size);

//nl_tuopbf_cmd_unicast_retry:
	res = nlmsg_unicast(nl_tusk, skb_out, tu_pid);
	if (res == -11 && print_first_buff_err==0) {
	    printk(KERN_INFO "func %s, note: Error while sending buff to use\n", __func__);
	    print_first_buff_err = 0;
	    msleep(1000);
	    //res = nlmsg_unicast(nl_tusk, skb_out, tu_pid);
	    //goto nl_tuopbf_cmd_unicast_retry;
	}
	//nlmsg_free(skb_out);
    //} while (res < 0);

    return res;
}
/**/				// ++ platero, end

/* xen tmem foundation ops/hypercalls */

static inline int xen_tmem_op(u32 tmem_cmd, u32 tmem_pool, struct tmem_oid oid,
	u32 index, unsigned long gmfn, u32 tmem_offset, u32 pfn_offset, u32 len)
{
	struct tmem_op op;
	int rc = 0;

	op.cmd = tmem_cmd;
	op.pool_id = tmem_pool;
	op.u.gen.oid[0] = oid.oid[0];
	op.u.gen.oid[1] = oid.oid[1];
	op.u.gen.oid[2] = oid.oid[2];
	op.u.gen.index = index;
	op.u.gen.tmem_offset = tmem_offset;
	op.u.gen.pfn_offset = pfn_offset;
	op.u.gen.len = len;
	set_xen_guest_handle(op.u.gen.gmfn, (void *)gmfn);
	rc = HYPERVISOR_tmem_op(&op);
	return rc;
}

static int xen_tmem_new_pool(struct tmem_pool_uuid uuid,
				u32 flags, unsigned long pagesize)
{
	struct tmem_op op;
	int rc = 0, pageshift;

	for (pageshift = 0; pagesize != 1; pageshift++)
		pagesize >>= 1;
	flags |= (pageshift - 12) << TMEM_POOL_PAGESIZE_SHIFT;
	flags |= TMEM_SPEC_VERSION << TMEM_VERSION_SHIFT;
	op.cmd = TMEM_NEW_POOL;
	op.u.new.uuid[0] = uuid.uuid_lo;
	op.u.new.uuid[1] = uuid.uuid_hi;
	op.u.new.flags = flags;
	rc = HYPERVISOR_tmem_op(&op);
	return rc;
}


static int xen_tmem_server_remote_status(int request_get_page, void *page_to_put)
{
        int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

        pop->cmd = TMEM_SERVER_REMOTE_STATUS;
        pop->u.remote.max_put_pages = page_to_put ? 1 : 0; // Xen could deduce from page_ptr_tmp
        pop->u.remote.max_get_pages = request_get_page;
        pop->u.remote.page_ptr_tmp = (uint64_t)page_to_put;
        rc = HYPERVISOR_tmem_op(pop);
        return rc;
}

static int xen_tmem_client_remote_status(void)
{
        int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

        pop->cmd = TMEM_CLIENT_REMOTE_STATUS;
        rc = HYPERVISOR_tmem_op(pop);
        return rc;
}

static int xen_tmem_got_page(uint64_t block_id, void *page_got)
{
        int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

        pop->cmd = TMEM_GOT_PAGE;
        pop->u.remote.page_ptr_tmp = (uint64_t)page_got;
        pop->u.remote.got_block_id = block_id;
        rc = HYPERVISOR_tmem_op(pop);
        return rc;
}

/**/				// ++ platero. start
static int xen_tmem_readstats(struct tmop_cnt *page_to_copy)
{
        int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

        pop->cmd = TMEM_READSTATS;
        pop->u.tmemstats.page_ptr_tmp = (uint64_t)page_to_copy;
        pop->u.tmemstats.block_id = 0;
        rc = HYPERVISOR_tmem_op(pop);
        return rc;
}

static int xen_tmem_rsvpg(struct mm_hyp_cmd *cmd_page)
{
        int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

        pop->cmd = TMEM_RSVPG;
	pop->u.tmemstats.page_ptr_tmp = (uint64_t)cmd_page;
	pop->u.tmemstats.block_id = 0;

        rc = HYPERVISOR_tmem_op(pop);
        return rc;
}

static int xen_tmem_sethninf(struct mm_hyp_cmd *cmd_page) 
{
	int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

	pop->cmd = TMEM_SET_HYP_NODE_INF;
	pop->u.tmem_hypnodinf.addr_strt = cmd_page->m.pg_mng[0].mm_target;
	pop->u.tmem_hypnodinf.region_id = cmd_page->m.pg_mng[0].dom_id;
	pop->u.tmem_hypnodinf.num_pages = cmd_page->m.pg_mng[0].dn_target;
	pop->u.tmem_hypnodinf.fraction_resolution = cmd_page->m.pg_mng[0].dn_target_prev;
	pop->u.tmem_hypnodinf.policy = cmd_page->m.pg_mng[0].mm_dn_slk;

	//printk("in %s, about to hyper-call\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);
        return rc;
}

static int xen_tmem_settfal(struct mm_hyp_cmd *cmd_page) 
{
	int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

	pop->cmd = TMEM_SET_TOTAL_LIST;
	pop->u.tmemstats.page_ptr_tmp = (uint64_t)cmd_page;
	pop->u.tmemstats.block_id = 0;
	
	//printk("in %s, about to hyper-call\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);
        return rc;
}

static int xen_tmem_setlfal(struct mm_hyp_cmd *cmd_page) 
{
	int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

	pop->cmd = TMEM_SET_LOCAL_LIST;
	pop->u.tmem_hypnodinf.num_pages = cmd_page->m.pglst_mng.len;
	
	//printk("in %s, about to hyper-call\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);
        return rc;
}

static int xen_tmem_mov_addresses(struct mm_hyp_cmd *cmd_page) 
{
	int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

	switch (cmd_page->cmd) {
	    case MM_TFAL_TO_LFAL:
	 	pop->cmd = TMEM_MOVE_TOTAL_TO_LOCAL;
	    break;
	    case MM_LFAL_TO_TFAL:
		pop->cmd = TMEM_MOVE_LOCAL_TO_TOTAL;
	    break;
	    case MM_TFAL_TO_RFAL:
		pop->cmd = TMEM_MOVE_TOTAL_TO_REMOTE;
	    break;
	    case MM_RFAL_TO_TFAL:
		pop->cmd = TMEM_MOVE_REMOTE_TO_TOTAL;
	    break;
	}
	pop->u.tmem_hypnodinf.num_pages = cmd_page->m.pglst_mng.len;
	pop->u.tmem_hypnodinf.addr_strt = (uint64_t)cmd_page->m.pglst_mng.src_group;
	pop->u.tmem_hypnodinf.region_id = (uint32_t)cmd_page->m.pglst_mng.dst_group;
	
	printk("in %s, about to hypercall\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);
        return rc;
}

static int xen_tmem_req_mcap(struct mm_hyp_cmd *cmd_page) {
	int rc = 0, i=0;
        struct tmem_op *pop = (struct tmem_op *)status_page;
	struct mm_hyp_cmd* tmcmd=NULL;

	pop->cmd = TMEM_REQUEST_FROM_REMOTE;
	pop->u.tmem_hypnodinf.region_id = cmd_page->m.pglst_mng.region_id;
	pop->u.tmem_hypnodinf.num_pages = cmd_page->m.pglst_mng.len;
	pop->u.tmem_hypnodinf.addr_strt = (uint64_t)cmd_page;

	printk("func %s, note: about to hypercall\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);

	printk("func%s, note: About to call nl_resp_mmhyp_cmd\n", __func__);
	tmcmd = (struct mm_hyp_cmd*)pop->u.tmem_hypnodinf.addr_strt;
	printk("\ttmcmd->m.pglst_mng.len=%lu, .region_id=%i\n", (long unsigned int)tmcmd->m.pglst_mng.len, 
			tmcmd->m.pglst_mng.region_id);
	for (i=0; i < tmcmd->m.pglst_mng.len; i++) {
	    printk("\t\ttmcmd->m.pglst_mng.addr_blks[%i], .base=%lu, .length=%lu\n", 
			i, (long unsigned int)tmcmd->m.pglst_mng.addr_blks[i].base, 
				(long unsigned int)tmcmd->m.pglst_mng.addr_blks[i].length);
	}
	nl_resp_mmhyp_cmd((struct mm_hyp_cmd*)pop->u.tmem_hypnodinf.addr_strt, 
				sizeof(struct mm_hyp_cmd) );

        return rc;
}

static int xen_tmem_rcv_mcap(struct mm_hyp_cmd *cmd_page) {
	int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

	pop->cmd = TMEM_RECEIVED_FROM_REMOTE;
	pop->u.tmemstats.page_ptr_tmp = (uint64_t)cmd_page;
	pop->u.tmemstats.block_id = 0;

	printk("in %s, about to hypercall\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);

	return rc;
}

static int xen_tmem_relinq_mcap(struct mm_hyp_cmd *cmd_page) {
	int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

	pop->cmd = TMEM_RELINQUISH_REMOTE;
	pop->u.tmem_hypnodinf.num_pages = cmd_page->m.pglst_mng.len;
	pop->u.tmem_hypnodinf.region_id = cmd_page->m.pglst_mng.region_id;
	pop->u.tmem_hypnodinf.addr_strt = (uint64_t)cmd_page;

	printk("in %s, about to hypercall\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);

	printk("About to call nl_resp_mmhyp_cmd\n");
	nl_resp_mmhyp_cmd((struct mm_hyp_cmd*)pop->u.tmem_hypnodinf.addr_strt, 
				sizeof(struct mm_hyp_cmd) );

	return rc;
}

static int xen_tmem_relack_mcap(struct mm_hyp_cmd *cmd_page) {
	int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

	pop->cmd = TMEM_RELACK_REMOTE;
	pop->u.tmemstats.page_ptr_tmp = (uint64_t)cmd_page;
	pop->u.tmemstats.block_id = 0;

	printk("in %s, about to hypercall\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);

	return rc;
}

static int xen_tmem_reclaim_mcap(struct mm_hyp_cmd *cmd_page) {
	int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

	pop->cmd = TMEM_RECLAIM_REMOTE;
	pop->u.tmem_hypnodinf.num_pages = cmd_page->m.pglst_mng.len;
	pop->u.tmem_hypnodinf.region_id = cmd_page->m.pglst_mng.region_id;
	pop->u.tmem_hypnodinf.addr_strt = (uint64_t)cmd_page;

	printk("in %s, about to hypercall\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);

	printk("About to call nl_resp_mmhyp_cmd\n");
	nl_resp_mmhyp_cmd((struct mm_hyp_cmd*)pop->u.tmem_hypnodinf.addr_strt, 
				sizeof(struct mm_hyp_cmd) );

	return rc;
}

static int xen_tmem_reclack_mcap(struct mm_hyp_cmd *cmd_page) {
	int rc = 0;
        struct tmem_op *pop = (struct tmem_op *)status_page;

	pop->cmd = TMEM_RECLACK_REMOTE;
	pop->u.tmemstats.page_ptr_tmp = (uint64_t)cmd_page;
	pop->u.tmemstats.block_id = 0;

	printk("in %s, about to hypercall\n", __func__);
	rc = HYPERVISOR_tmem_op(pop);

	return rc;
}

#ifdef TKM_DBG
static int test_commands_received = 0;
#endif

static int process_command(struct mm_hyp_cmd *mmcmd, uint32_t pid, uint32_t msg_size)
{
    #ifdef TKM_DBG
    struct tmop_cnt opcnt;
    #endif
            switch (mmcmd->cmd) { 
		case MM_TEST:
                    //uint64_t block_id = remote_get_ack.op.block_id;
                    //printk("now have block %llx\n", block_id);
		    //xen_tmem_got_page(block_id, remote_get_ack.buf);
		    printk("Received MM_TEST command from user\n");
		    #ifdef TKM_DBG
		    if ( (test_commands_received % 2) == 0 ) {
			opcnt.domcnt = 27;
		    } else if ( (test_commands_received % 2) == 1) {
			opcnt.domcnt = 69;
		    }
		    nl_send_msg(&opcnt, msg_size);
		    test_commands_received++;
		    #endif
		break;
		case MM_RSVPAG:
		case MM_RELPAG:
		    printk("MM_RELPAG | MM_RSVPAG\n");
		    xen_tmem_rsvpg(mmcmd);
		break;
		case MM_SETUP_HNINF:
		    mm_pid = pid;
		    printk("MM_SETUP_HNINF, mm_pid=%i\n", mm_pid);
		    xen_tmem_sethninf(mmcmd);
		break;
		case MM_SETUP_TFAL:
		    printk("MM_SETUP_TFAL\n");
		    xen_tmem_settfal(mmcmd);
		break;
	 	case MM_SETUP_LFAL:
		    printk("MM_SETUP_LFAL\n");
		    xen_tmem_setlfal(mmcmd);
		break;
		case MM_TFAL_TO_LFAL:
		    printk("MM_TFAL_TO_LFAL\n");
		    xen_tmem_mov_addresses(mmcmd);
		break;
		case MM_LFAL_TO_TFAL:
		    printk("MM_LFAL_TO_TFAL\n");
		    xen_tmem_mov_addresses(mmcmd);
		break;
		case MM_TFAL_TO_RFAL:
		    printk("MM_TFAL_TO_RFAL\n");
		    xen_tmem_mov_addresses(mmcmd);
		break;
		case MM_RFAL_TO_TFAL:
		    printk("MM_RFAL_TO_TFAL\n");
		    xen_tmem_mov_addresses(mmcmd);
		break;
		case MM_REQ_MCAP:
		    printk("MM_REQ_MCAP\n");
		    xen_tmem_req_mcap(mmcmd);
		break;
		case MM_RCV_MCAP:
		    printk("MM_RCV_MCAP\n");
		    xen_tmem_rcv_mcap(mmcmd);
		break;
		case MM_RELINQ_MCAP:
		    printk("MM_RELINQ_MCAP\n");
		    xen_tmem_relinq_mcap(mmcmd);
		break;
		case MM_RELACK_MCAP:
		    printk("MM_RELACK_MCAP\n");
		    xen_tmem_relack_mcap(mmcmd);
		break;
		case MM_RECLAIM_MCAP:
		    printk("MM_RECLAIM_MCAP\n");
		    xen_tmem_reclaim_mcap(mmcmd);
		break;
		case MM_RECLACK_MCAP:
		    printk("MM_RECLACK_MCAP\n");
		    xen_tmem_reclack_mcap(mmcmd);
		break;
		case MM_SEND_STATS:
		    printk("MM_SEND_STATS, pid=%i, msg_size=%i, mmcmd->m.pglst_mng.len=%lu\n", 
				mm_pid, msg_size, (long unsigned int)mmcmd->m.pglst_mng.len);
		    mmcmd->cmd = MM_TEST;
		break;
	    }
        
	return 0;//count;
}

static int tuproc_cmd(struct remote_reply *rop, uint32_t pid, uint32_t msg_size)
{
    unsigned long flags;

    switch (rop->op.cmd) {
	case REMOTE_OP_PID:
	    tu_pid = pid;
	    #ifdef TKM_DBG
	    printk("REMOTE_OP_PID, tu_pid=%i, rop->op.block_id=%i (pid)\n", 
			tu_pid, (unsigned int)rop->op.block_id);
	    #endif
	break;
	case REMOTE_OP_PUT_KERNACK:
	    #ifdef TKM_DBG
	    printk("REMOTE_OP_PUT_KERNACK\n");
	    #endif
	    tuopready = 1;
	    wake_up_interruptible(&tuopwait);
	break;
	case REMOTE_OP_GET_KERNACK:
	    #ifdef TKM_DBG
	    printk("REMOTE_OP_GET_KERNACK\n");
	    #endif
	    tuopready = 1;
	    wake_up_interruptible(&tuopwait);
	break;
	case REMOTE_OP_GETOK:
	    spin_lock_irqsave(&remote_spinlock, flags);
	    xen_tmem_got_page(rop->op.block_id, rop->buf);
	    spin_unlock_irqrestore(&remote_spinlock, flags);
	    #ifdef TKM_DBG
	    printk("REMOTE_OP_GETOK, rop->op.block_id=%lu\n", 
			(long unsigned int)rop->op.block_id);
	    #endif
	    tuopready = 1;
	    wake_up_interruptible(&tuopwait);
	break;
	case REMOTE_OP_GETFAIL:
	    printk("REMOTE_OP_GETFAIL, rop->op.block_id=%lu\n", 
			(long unsigned int)rop->op.block_id);
	    tuopready = 1;
	    wake_up_interruptible(&tuopwait);
	break;
    }

    return 1;
}

static void nl_data_ready(struct sk_buff *skb) {
    struct nlmsghdr *nlh;
    uint32_t msg_size;
    int res;

    struct mm_hyp_cmd *mmcmd;

    //printk(KERN_INFO "Entering: %s, mmcmd=%lu\n", __FUNCTION__, mmcmd);

    //msg_size = strlen(msg);
    msg_size = (uint32_t)sizeof(struct mm_hyp_cmd);

    nlh=(struct nlmsghdr*)skb->data;
    //memcpy(&mmcmd, (struct mm_hyp_cmd *)nlmsg_data(nlh), msg_size);
    mmcmd = (struct mm_hyp_cmd*)nlmsg_data(nlh);
    printk(KERN_INFO "%i. Command received:%i\n", mmcmd->cmd, trans_count);
    trans_count++;

    res = process_command(mmcmd, (uint32_t)nlh->nlmsg_pid, msg_size);
}

static void nl_turdy(struct sk_buff *skb) {
    struct nlmsghdr *nlh;
    uint32_t msg_size;
    int res;

    struct remote_reply *rop;

    msg_size = (uint32_t)sizeof(struct remote_reply);
    nlh=(struct nlmsghdr*)skb->data;
    //memcpy(&mmcmd, (struct mm_hyp_cmd *)nlmsg_data(nlh), msg_size);
    rop = (struct remote_reply*)nlmsg_data(nlh);
    printk(KERN_INFO "func %s, note: rop->op.cmd=%i, nlh->nlmsg_pid=%i. Command received:%i\n", 
			__func__, rop->op.cmd, (uint32_t)nlh->nlmsg_pid, trans_count);
    trans_count++;

    res = tuproc_cmd(rop, (uint32_t)nlh->nlmsg_pid, msg_size);
}

/**/				// ++ platero. end

/* xen generic tmem ops */

static int xen_tmem_put_page(u32 pool_id, struct tmem_oid oid,
			     u32 index, unsigned long pfn)
{
	unsigned long gmfn = xen_pv_domain() ? pfn_to_mfn(pfn) : pfn;

	printk("About to TMEM_PUT_PAGE\n");
	return xen_tmem_op(TMEM_PUT_PAGE, pool_id, oid, index,
		gmfn, 0, 0, 0);
}

static int xen_tmem_get_page(u32 pool_id, struct tmem_oid oid,
			     u32 index, unsigned long pfn)
{
	unsigned long gmfn = xen_pv_domain() ? pfn_to_mfn(pfn) : pfn;

	return xen_tmem_op(TMEM_GET_PAGE, pool_id, oid, index,
		gmfn, 0, 0, 0);
}

static int xen_tmem_flush_page(u32 pool_id, struct tmem_oid oid, u32 index)
{
	return xen_tmem_op(TMEM_FLUSH_PAGE, pool_id, oid, index,
		0, 0, 0, 0);
}

static int xen_tmem_flush_object(u32 pool_id, struct tmem_oid oid)
{
	return xen_tmem_op(TMEM_FLUSH_OBJECT, pool_id, oid, 0, 0, 0, 0, 0);
}


#ifdef CONFIG_CLEANCACHE
static int xen_tmem_destroy_pool(u32 pool_id)
{
	struct tmem_oid oid = { { 0 } };

	return xen_tmem_op(TMEM_DESTROY_POOL, pool_id, oid, 0, 0, 0, 0, 0);
}

/* cleancache ops */

static void tmem_cleancache_put_page(int pool, struct cleancache_filekey key,
				     pgoff_t index, struct page *page)
{
	u32 ind = (u32) index;
	struct tmem_oid oid = *(struct tmem_oid *)&key;
	unsigned long pfn = page_to_pfn(page);

	if (pool < 0)
		return;
	if (ind != index)
		return;
	mb(); /* ensure page is quiescent; tmem may address it with an alias */
	(void)xen_tmem_put_page((u32)pool, oid, ind, pfn);
}

static int tmem_cleancache_get_page(int pool, struct cleancache_filekey key,
				    pgoff_t index, struct page *page)
{
	u32 ind = (u32) index;
	struct tmem_oid oid = *(struct tmem_oid *)&key;
	unsigned long pfn = page_to_pfn(page);
	int ret;

	/* translate return values to linux semantics */
	if (pool < 0)
		return -1;
	if (ind != index)
		return -1;
	ret = xen_tmem_get_page((u32)pool, oid, ind, pfn);
	if (ret == 1)
		return 0;
        else if (ret == 2)
        {
            printk("Cleancache should not be being used!\n");
            BUG_ON(ret == 2); // remote pages for cleancache NOT implemented yet
            return 0;
        }
	else
		return -1;
}

static void tmem_cleancache_flush_page(int pool, struct cleancache_filekey key,
				       pgoff_t index)
{
	u32 ind = (u32) index;
	struct tmem_oid oid = *(struct tmem_oid *)&key;

	if (pool < 0)
		return;
	if (ind != index)
		return;
	(void)xen_tmem_flush_page((u32)pool, oid, ind);
}

static void tmem_cleancache_flush_inode(int pool, struct cleancache_filekey key)
{
	struct tmem_oid oid = *(struct tmem_oid *)&key;

	if (pool < 0)
		return;
	(void)xen_tmem_flush_object((u32)pool, oid);
}

static void tmem_cleancache_flush_fs(int pool)
{
	if (pool < 0)
		return;
	(void)xen_tmem_destroy_pool((u32)pool);
}

static int tmem_cleancache_init_fs(size_t pagesize)
{
	struct tmem_pool_uuid uuid_private = TMEM_POOL_PRIVATE_UUID;

	return xen_tmem_new_pool(uuid_private, 0, pagesize);
}

static int tmem_cleancache_init_shared_fs(char *uuid, size_t pagesize)
{
	struct tmem_pool_uuid shared_uuid;

	shared_uuid.uuid_lo = *(u64 *)uuid;
	shared_uuid.uuid_hi = *(u64 *)(&uuid[8]);
	return xen_tmem_new_pool(shared_uuid, TMEM_POOL_SHARED, pagesize);
}

static struct cleancache_ops tmem_cleancache_ops = {
	.put_page = tmem_cleancache_put_page,
	.get_page = tmem_cleancache_get_page,
	.invalidate_page = tmem_cleancache_flush_page,
	.invalidate_inode = tmem_cleancache_flush_inode,
	.invalidate_fs = tmem_cleancache_flush_fs,
	.init_shared_fs = tmem_cleancache_init_shared_fs,
	.init_fs = tmem_cleancache_init_fs
};
#endif

#ifdef CONFIG_FRONTSWAP
/* frontswap tmem operations */

/* a single tmem poolid is used for all frontswap "types" (swapfiles) */
static int tmem_frontswap_poolid;

/*
 * Swizzling increases objects per swaptype, increasing tmem concurrency
 * for heavy swaploads.  Later, larger nr_cpus -> larger SWIZ_BITS
 */
#define SWIZ_BITS		4
#define SWIZ_MASK		((1 << SWIZ_BITS) - 1)
#define _oswiz(_type, _ind)	((_type << SWIZ_BITS) | (_ind & SWIZ_MASK))
#define iswiz(_ind)		(_ind >> SWIZ_BITS)
#define tswiz(_oid)             (_oid[0] >> SWIZ_BITS)
#define indswiz(_oid, _index)   ((_index << SWIZ_BITS) | (_oid[0] & SWIZ_MASK))

static inline struct tmem_oid oswiz(unsigned type, u32 ind)
{
	struct tmem_oid oid = { .oid = { 0 } };
	oid.oid[0] = _oswiz(type, ind);
	return oid;
}

/* returns 0 if the page was successfully put into frontswap, -1 if not */
static int tmem_frontswap_store(unsigned type, pgoff_t offset,
				   struct page *page)
{
	u64 ind64 = (u64)offset;
	u32 ind = (u32)offset;
	unsigned long pfn = page_to_pfn(page);
	int pool = tmem_frontswap_poolid;
	int ret;

	if (pool < 0)
		return -1;
	if (ind64 != ind)
		return -1;
	mb(); /* ensure page is quiescent; tmem may address it with an alias */
	ret = xen_tmem_put_page(pool, oswiz(type, ind), iswiz(ind), pfn);
	/* translate Xen tmem return values to linux semantics */
	if (ret == 1)
		return 0;
	else
		return -1;
}

/*
 * returns 0 if the page was successfully gotten from frontswap, -1 if
 * was not present (should never happen!)
 */
static int tmem_frontswap_load(unsigned type, pgoff_t offset,
				   struct page *page)
{
	u64 ind64 = (u64)offset;
	u32 ind = (u32)offset;
	unsigned long pfn = page_to_pfn(page);
	int pool = tmem_frontswap_poolid;
	int ret;

	if (pool < 0)
		return -1;
	if (ind64 != ind)
		return -1;
	ret = xen_tmem_get_page(pool, oswiz(type, ind), iswiz(ind), pfn);
	/* translate Xen tmem return values to linux semantics */
	if (ret == 1)
		return 0;
        else if (ret == 2)
        {
            unsigned long flags;
            struct waiting_page_t *wp; 
            // printk("PAGE is remote: %x %x %x, page=%p\n", pool, type, ind, page);
            wp = kmalloc(sizeof(struct waiting_page_t), 0);
            wp->pool = pool;
            wp->type = type;
            wp->ind = ind;
            wp->page = page;
            spin_lock_irqsave(&remote_spinlock, flags);
            list_add(&wp->waiting_pages, &get_waiting_pages_list);
            spin_unlock_irqrestore(&remote_spinlock, flags);
            return 2;
        }
	else
		return -1;
}

/* flush a single page from frontswap */
static void tmem_frontswap_flush_page(unsigned type, pgoff_t offset)
{
	u64 ind64 = (u64)offset;
	u32 ind = (u32)offset;
	int pool = tmem_frontswap_poolid;

	if (pool < 0)
		return;
	if (ind64 != ind)
		return;
	(void) xen_tmem_flush_page(pool, oswiz(type, ind), iswiz(ind));
}

/* flush all pages from the passed swaptype */
static void tmem_frontswap_flush_area(unsigned type)
{
	int pool = tmem_frontswap_poolid;
	int ind;

	if (pool < 0)
		return;
	for (ind = SWIZ_MASK; ind >= 0; ind--)
		(void)xen_tmem_flush_object(pool, oswiz(type, ind));
}

static void tmem_frontswap_init(unsigned ignored)
{
	struct tmem_pool_uuid private = TMEM_POOL_PRIVATE_UUID;

	/* a single tmem poolid is used for all frontswap "types" (swapfiles) */
	if (tmem_frontswap_poolid < 0)
		tmem_frontswap_poolid =
		    xen_tmem_new_pool(private, TMEM_POOL_PERSIST, PAGE_SIZE);
}

static struct frontswap_ops tmem_frontswap_ops = {
	.store = tmem_frontswap_store,
	.load = tmem_frontswap_load,
	.invalidate_page = tmem_frontswap_flush_page,
	.invalidate_area = tmem_frontswap_flush_area,
	.init = tmem_frontswap_init
};
#endif

struct remote_status_return_t
{
    uint8_t ok;
    uint8_t put_request;
    uint8_t get_request;
    uint8_t put_is_del;
    uint64_t put_global_address;
    uint64_t get_global_address;
};


static void xen_mem_event_server(void)
{
    struct remote_status_return_t *retval = (struct remote_status_return_t *)status_page;
    void *page_to_put = NULL;
    //int space = 0;
    int request_get_page = 0;
    int request_put_page = 0;
    struct temp_page_t *tp = NULL;
    int head, tail;
    struct remote_op op;
    int32_t rc=0, i=0;
    //unsigned long flags;

    // must be space in the pending ops list
    //printk("func %s, note: interrupt come back\n", __func__);
    //space = CIRC_SPACE(pending_ops.head, pending_ops.tail, MAX_PENDING_OPS);

    //if (space >= 2)
        /* request both */
        request_get_page = 1;
        request_put_page = 1;
    //}

    // must be a free page to receive the data
    if(list_empty(&free_temp_pages_list))
        request_put_page = 0;

    if (request_put_page > 0)
    {
        tp = list_first_entry(&free_temp_pages_list, struct temp_page_t, free_temp_pages);
        list_del(&tp->free_temp_pages);  // if no response or is a del, will later put it back
        page_to_put = tp->page_ptr;
    }

    // printk("request_put_page: %d\n", request_put_page);

    retval->ok = 2; // just to check
    xen_tmem_server_remote_status(request_get_page, page_to_put);
    BUG_ON (retval->ok != 1);

    head = 0; //pending_ops.head;
    tail = 0; //pending_ops.tail;
    if (retval->get_request)
    {
        //int curr_pos;
        BUG_ON(!request_get_page);
        /*BUG_ON(CIRC_SPACE(head, tail, MAX_PENDING_OPS) == 0);

        curr_pos = head;*/
#if 0
        /* Allow gets to "overtake" puts */
        while (CIRC_CNT(curr_pos, tail, MAX_PENDING_OPS) > 1) /* do not move past first, which may be curr_op */
        {
            int prev_pos = (curr_pos - 1) & (MAX_PENDING_OPS-1);
            if (pending_ops.ops[prev_pos].op.block_id == retval->get_global_address)
            {
                /* We cannot move the get before a put to the same address */
                /* Don't move past a get either, which is either the last or blocked by a put */
                break;
            }
            pending_ops.ops[curr_pos] = pending_ops.ops[prev_pos];
            curr_pos = prev_pos;
        }
#endif
        /*pending_ops.ops[curr_pos].op.cmd = REMOTE_OP_GET;
        pending_ops.ops[curr_pos].op.block_id = retval->get_global_address;
        pending_ops.ops[curr_pos].temp_page_if_put = NULL;
        head = (head+1) & (MAX_PENDING_OPS-1);*/

	//spin_lock_irqsave(&remote_spinlock, flags);
	op.cmd = REMOTE_OP_GET;
	op.block_id = retval->get_global_address;
//nl_tuop_cmd_retry1:
	rc = nl_tuop_cmd(&op, sizeof(struct remote_op));
	tuopready = 0;
	//if (rc == -11) {
	    //i = 0;
	    //while (i < 100) {
		//i=i+1;
	    //}
	    //goto nl_tuop_cmd_retry1;
	//}
	//spin_unlock_irqrestore(&remote_spinlock, flags);

	
	    //printk("func %s, note: nothing pending, spinlock taken\n", __func__);
            //ret = wait_event_interruptible(procwait, procready != 0);

	printk("func %s, note: retval->get_request, after nl_tuop_cmd, MAX_PENDING_OPS=%i, head=%i, rc=%i\n", 
		__func__, MAX_PENDING_OPS, head, rc);

        procready = 1;
    }
    
    if (page_to_put && retval->put_request)
    {
        WARN_ON(!tp);
        /*BUG_ON (CIRC_SPACE(head, tail, MAX_PENDING_OPS) == 0);

        pending_ops.ops[head].op.cmd = retval->put_is_del ? REMOTE_OP_DEL : REMOTE_OP_PUT;
        pending_ops.ops[head].op.block_id = retval->put_global_address;
        pending_ops.ops[head].temp_page_if_put = retval->put_is_del ? NULL : tp;
        head = (head+1) & (MAX_PENDING_OPS-1);*/

	//spin_lock_irqsave(&remote_spinlock, flags);
	op.cmd =  retval->put_is_del ? REMOTE_OP_DEL : REMOTE_OP_PUT;
	op.block_id = retval->put_global_address;
//nl_tuop_cmd_retry2:
	rc = nl_tuop_cmd(&op, sizeof(struct remote_op));
	tuopready = 0;
	//if (rc == -11) {
	    //i = 0;
	    //while (i < 100) {
		//i=i+1;
	    //}
	    //goto nl_tuop_cmd_retry2;
	//}
	//spin_unlock_irqrestore(&remote_spinlock, flags);

	//printk("func %s, note: retval->put_request, after nl_tuop_cmd, MAX_PENDING_OPS=%i, head=%i, rc=%i\n", 
	//	__func__, MAX_PENDING_OPS, head, rc);

	BUG_ON(!tp);	
	//spin_lock_irqsave(&remote_spinlock, flags);
	if (tp) {
//nl_tuopbf_cmd_retry1:
	    rc = nl_tuopbf_cmd(tp->page_ptr, 4096);
	    //if (rc == -11) {
		//i = 0;
		//while (i < 100) {
		    //i=i+1;
		//}
		//goto nl_tuopbf_cmd_retry1;
	    //}
	    list_add(&tp->free_temp_pages, &free_temp_pages_list);
	}
	//spin_unlock_irqrestore(&remote_spinlock, flags);

	//printk("func %s, note: after tp check, page_to_put, MAX_PENDING_OPS=%i, head=%i, rc=%i\n", 
	//	__func__, MAX_PENDING_OPS, head, rc);

	procready = 1;
    }

    pending_ops.head = head;

    if ( tp &&
            (!retval->put_request ||   //   No put
            retval->put_is_del) )      //   Put was actually a delete
        {
            // didn't use tp
            list_add(&tp->free_temp_pages, &free_temp_pages_list);
        }

}

/**/					// ++ platero, start
static void xen_tmem_event_server(void)
{
    //struct remote_status_return_t *retval = (struct remote_status_return_t *)status_page;
    struct tmop_cnt *page_to_put = NULL;
    int rc;

    page_to_put = (struct tmop_cnt *) kmalloc(sizeof(struct tmop_cnt), 0);

    rc = xen_tmem_readstats(page_to_put);
    if (rc) {
	printk("xen_tmem_readstats failed to execute the hypercall successfully\n");
    }

    //printk("/******** Entry ********/\n");
    //printk("\topcnt.domcnt=%i\n", page_to_put->domcnt);
    //for (i=0; i < page_to_put->domcnt; i++) {
	//printk("\tfunc %s, note: page_to_put->dominfo[%i].region_id=%i, page_to_put->dominfo[%i].dom_id=%i", 
	//		__func__, i , page_to_put->dominfo[i].region_id, 
	//			  i, page_to_put->dominfo[i].dom_id);  
    //}

    nl_send_msg(page_to_put, sizeof(struct tmop_cnt) );

    kfree(page_to_put);
    procready_tmem = 1;
}
/**/					// ++ platero, end







static void xen_mem_event_server_work_fn(struct work_struct *work)
{
    unsigned long flags;
    int ret;

    ret = wait_event_interruptible(tuopwait, tuopready == 1);

    //spin_lock_irqsave(&remote_spinlock, flags); // all in spin lock due to status_page
    spin_lock(&remote_spinlock);
    xen_mem_event_server();
    //spin_unlock_irqrestore(&remote_spinlock, flags);
    spin_unlock(&remote_spinlock);

    //if (procready)
        //wake_up_interruptible(&procwait);
}

/**/					// ++ platero, start
static void xen_tmem_event_server_work_fn(struct work_struct *work)
{
    unsigned long flags;
    spin_lock_irqsave(&remote_tstats_spinlock, flags); // all in spin lock due to status_page
    xen_tmem_event_server();
    spin_unlock_irqrestore(&remote_tstats_spinlock, flags);

    if (procready_tmem)
        wake_up_interruptible(&procwait_tmem);
}
/**/					// ++ platero, end


struct got_t
{
    u8 is_valid;
    u32 pool_id;
    u32 index;
    u64 oid[3];
};
#define MAX_GOT_T (4096 / sizeof(struct got_t) )

static void xen_mem_event_client_work_fn(struct work_struct *work)
{
    struct got_t *got_pages = (struct got_t *)status_page;
    unsigned long flags;
    int i;
    // printk("tmem: client interrupt\n");
    // struct remote_status_return_t *retval = (struct remote_status_return_t *)status_page;

    spin_lock_irqsave(&remote_spinlock, flags); // all in spin lock due to status_page
    xen_tmem_client_remote_status();

    for(i = 0; i < MAX_GOT_T; i++)
    {
        struct got_t *gt = &got_pages[i];
        struct waiting_page_t *wp;
        struct waiting_page_t *wp_found = NULL;
        int type;
        int ind;
        if (!gt->is_valid)
            break;
        /* printk("got remote PAGE: %x %llx %llx %llx %x\n", 
                gt->pool_id, 
                gt->oid[0], gt->oid[1], gt->oid[2],
                gt->index); */
        type = tswiz(gt->oid);
        ind = indswiz(gt->oid, gt->index);
        // printk("got remote PAGE: %x %x %x\n", gt->pool_id, type, ind);

        // now try to find it (slow)
        list_for_each_entry(wp, &get_waiting_pages_list, waiting_pages)
        {
            // printk("compare with %x %x %x\n", wp->pool, wp->type, wp->ind);
            {
                if ( (wp->pool == gt->pool_id) &&
                        (wp->type == type) &&
                        (wp->ind == ind) )
                {
                    // printk("match for page %p\n", wp->page);
                    SetPageUptodate(wp->page);
                    unlock_page(wp->page);
                    wp_found = wp;
                    break;
                }
            }
        }
        if (wp_found)
        {
            list_del(&wp_found->waiting_pages);
            kfree(wp_found);
        }
    }
    spin_unlock_irqrestore(&remote_spinlock, flags);
}


static DECLARE_WORK(xen_mem_event_server_work, xen_mem_event_server_work_fn);
static DECLARE_WORK(xen_mem_event_client_work, xen_mem_event_client_work_fn);
/**/						// ++ platero, start
static DECLARE_WORK(xen_tmem_event_server_work, xen_tmem_event_server_work_fn);
/**/						// ++ platero, end

irqreturn_t xen_mem_event_interrupt_server(int irq, void *dev_id)
{
    schedule_work(&xen_mem_event_server_work);
    return IRQ_HANDLED;
}

/**/						// ++ platero, start
irqreturn_t xen_tmem_event_interrupt_server(int irq, void *dev_id)
{
    schedule_work(&xen_tmem_event_server_work);
    return IRQ_HANDLED;
}
/**/

irqreturn_t xen_mem_event_interrupt_client(int irq, void *dev_id)
{
    schedule_work(&xen_mem_event_client_work);
    return IRQ_HANDLED;
}

static int tmem_irq;
/**/				// ++ platero, start
static int tmem_stats_irq;
/**/				// ++ platero, end


// This is really dirty, as seq_file is not the right interface.
static struct pending_op_t *curr_op = NULL;
/**/				// ++ platero, start
//static struct pending_tmop_t *curr_tmop = NULL;
/**/				// ++ platero, end

static struct remote_reply remote_get_ack;
/**/				// ++ platero, start
//static struct mm_hyp_cmd mmcmd;
/**/				// ++ platero, end

static void *stat_seq_start(struct seq_file *s, loff_t *pos)
{
    //printk("func %s, note: curr_op=%lu\n", __func__, (long unsigned int)curr_op);
    return curr_op;
}

static void *stat_seq_next(struct seq_file *s, void *v, loff_t *pos)
{
    return NULL;
}

static int stat_seq_show(struct seq_file *m, void *v)
{
    // Send the operation
    seq_write(m, &curr_op->op, sizeof(struct remote_op));

    //printk("func %s, note: about to check curr_op=%lu\n", __func__, (long unsigned int)curr_op);
    if (curr_op->op.cmd == REMOTE_OP_PUT)
    {
	//printk("func %s, note: REMOTE_OP_PUT, about to seq_write\n", __func__);
        seq_write(m, curr_op->temp_page_if_put->page_ptr, 4096);
    }
    return 0;
}

static void stat_seq_stop(struct seq_file *s, void *v)
{
}

static struct seq_operations stat_seq_ops = {
    .start = stat_seq_start,
    .next = stat_seq_next,
    .stop = stat_seq_stop,
    .show = stat_seq_show
};

static int open_status(struct inode *inode, struct file *file)
{
    unsigned long flags;
    int tail;
    

    spin_lock_irqsave(&remote_spinlock, flags);
    tail = pending_ops.tail;

    //printk("func %s, note: opening tmem_remote\n", __func__);
    // Remove previous request
    if (curr_op)
    {
        // Done pending op
	//printk("func %s, note: curr_op not empty\n", __func__);
        if (curr_op->op.cmd == REMOTE_OP_PUT)
        {
            // Temp page is now free
	    //printk("func %s, note: curr_op is a put\n", __func__);
            BUG_ON(!curr_op->temp_page_if_put);
            list_add(&curr_op->temp_page_if_put->free_temp_pages, &free_temp_pages_list);
        }
	//printk("func %s, note: curr_op turns NULL\n", __func__);
        curr_op = NULL;
        tail = (tail + 1) & (MAX_PENDING_OPS-1);
        pending_ops.tail = tail;
    }

    do
    {
        procready = 0;
        if (CIRC_CNT(pending_ops.head, tail, MAX_PENDING_OPS) == 0)
        {
            // No pending operation; try to get one
	    // printk();	// generate a hypercall, when no cur_op
	    //printk("func %s, note: about to call xen_mem_event_server\n", __func__);
            xen_mem_event_server();
        }

        if (CIRC_CNT(pending_ops.head, tail, MAX_PENDING_OPS) > 0)
        {
            // Process pending operation
            curr_op = &pending_ops.ops[tail];
        }
        else
        {
            // Nothing pending 
            int ret;
	    //printk("func %s, note: nothing pending, before spinlock\n", __func__);
            spin_unlock_irqrestore(&remote_spinlock, flags);
	    //printk("func %s, note: nothing pending, spinlock taken\n", __func__);
            ret = wait_event_interruptible(procwait, procready != 0);
            spin_lock_irqsave(&remote_spinlock, flags);
	    //printk("func %s, note: nothing pending, spinlock released\n", __func__);
            if (ret)
            {
		printk("func %s, note: nothing pending, ret=%i\n", __func__, ret);
                // signalled
                break;
            }
        }

        // Keep trying until we get an operation, or 'wait_event_interruptible' gets
        // a signal (like user process dying)
    } while (!curr_op);

    spin_unlock_irqrestore(&remote_spinlock, flags);
    return seq_open(file, &stat_seq_ops);
}

// code adapted from comm_write in fs/proc/base.c
static ssize_t stat_write(struct file *file, const char __user *buf,
				size_t count, loff_t *offset)
{
        unsigned long flags;
	const size_t maxlen = sizeof(remote_get_ack);
        spin_lock_irqsave(&remote_spinlock, flags);

	// memset(buffer, 0, sizeof(remote_get_ack));
	if (copy_from_user(&remote_get_ack, buf, count > maxlen ? maxlen : count))
		return -EFAULT;

        if (count == sizeof(remote_get_ack))
        {
            if (remote_get_ack.op.cmd == REMOTE_OP_GETOK)
            {
                uint64_t block_id = remote_get_ack.op.block_id;
                // printk("now have block %llx\n", block_id);
                xen_tmem_got_page(block_id, remote_get_ack.buf);
            }
        }
        
        spin_unlock_irqrestore(&remote_spinlock, flags);
	return count;
}

static const struct file_operations status_file_fops =
{
    .owner = THIS_MODULE,
    .open = open_status,
    .read = seq_read,
    .write = stat_write,
    .release = seq_release
};

static int xen_tmem_init(void)
{
        struct remote_status_return_t *retval;
        int ret;
        int i;

	struct netlink_kernel_cfg cfg = { 
		.input = nl_data_ready,
	    };

	struct netlink_kernel_cfg tucfg = { 
		.input = nl_turdy,
	    };

	if (!xen_domain())
		return 0;

        status_page = (void *)get_zeroed_page(0); // doesn't have to be a page
        if (!status_page)
        {
            pr_info("Cannot allocate page - exiting\n");
            return -ENOMEM;
        }

        /* Test for compatible TMEM support */
        retval = (struct remote_status_return_t *)status_page;
        retval->ok = 2;
        ret = xen_tmem_server_remote_status(0, NULL);
        if (retval->ok != 1)
        {
            pr_info("tmem: no support for remote memory in Xen\n");
            return -ENODEV;
        }

        for(i=0; i<NUM_TEMP_PAGES; i++)
        {
            temp_pages[i].page_ptr = (void *)get_zeroed_page(0);
            if (!temp_pages[i].page_ptr)
            {
                pr_info("Cannot allocate page - exiting\n");
                return 0;
            }
            list_add(&temp_pages[i].free_temp_pages, &free_temp_pages_list);
        }

        if (xen_initial_domain())
        {
            // Remote page access server
            tmem_irq = bind_virq_to_irqhandler(VIRQ_MEM_EVENT, 0,
                                          xen_mem_event_interrupt_server, 0,
                                          "mem_event", NULL);

            pending_ops.head = 0;
            pending_ops.tail = 0;

	    /**/			// ++ platero, start
	    nl_sk = netlink_kernel_create(&init_net, NETLINK_USER, &cfg);
	    if (!nl_sk) {
		printk(KERN_ALERT "Error creating socket nl_sk.\n");
		return -10;
	    }

	    nl_tusk = netlink_kernel_create(&init_net, NETLINK_TMUSER, &tucfg);
	    if (!nl_tusk) {
		printk(KERN_ALERT "Error creating socket nl_tusk.\n");
		return -10;
	    }

	    tmem_stats_irq = bind_virq_to_irqhandler(VIRQ_TMEM_STATS, 0,
                                          xen_tmem_event_interrupt_server, 0,
                                          "mem_event", NULL);
	    /**/			// ++ platero, end

            pr_info("tmem: remote access server - disable frontswap and cleancache (for now)\n");
            frontswap = 0;
            cleancache = 0;
        }
        else
        {
            // Remote page access client
            tmem_irq = bind_virq_to_irqhandler(VIRQ_MEM_EVENT, 0,
                                          xen_mem_event_interrupt_client, 0,
                                          "mem_event", NULL);
            pr_info("tmem: remote access client\n");
        }

        pr_info("Bound VIRQ_MEM_EVENT to irq %d\n", tmem_irq);
	/**/					// ++ platero, start
	if (xen_initial_domain())
        {
	    pr_info("Bound VIRQ_TMEM_STATS to irq %d\n", tmem_stats_irq);
	}
	/**/					// ++ platero, end

#ifdef CONFIG_FRONTSWAP
	if (tmem_enabled && frontswap) {
		char *s = "";
		struct frontswap_ops *old_ops;

		tmem_frontswap_poolid = -1;
		old_ops = frontswap_register_ops(&tmem_frontswap_ops);
		if (IS_ERR(old_ops) || old_ops) {
			if (IS_ERR(old_ops))
                        {
                            ret = PTR_ERR(old_ops);
                            goto err;
                        }
			s = " (WARNING: frontswap_ops overridden)";
		}
		pr_info("frontswap enabled, RAM provided by Xen Transcendent Memory%s\n",
			s);
	}
#endif
#ifdef CONFIG_CLEANCACHE
        cleancache = 0; // PMC: only handling frontswap for now
	BUG_ON(sizeof(struct cleancache_filekey) != sizeof(struct tmem_oid));
	if (tmem_enabled && cleancache) {
		char *s = "";
		struct cleancache_ops *old_ops =
			cleancache_register_ops(&tmem_cleancache_ops);
		if (old_ops)
			s = " (WARNING: cleancache_ops overridden)";
		pr_info("cleancache enabled, RAM provided by Xen Transcendent Memory%s\n",
			s);
	}
#endif
#ifdef CONFIG_XEN_SELFBALLOONING
	/*
	 * There is no point of driving pages to the swap system if they
	 * aren't going anywhere in tmem universe.
	 */
	if (!frontswap) {
		selfshrinking = false;
		selfballooning = false;
	}
	//xen_selfballoon_init(selfballooning, selfshrinking);
#endif
	//Ballooning disabled
	xen_selfballoon_init(false, false);

        /* Communicate with user space using /proc/xen/tmem_remote */
        proc_create("tmem_remote", 0, NULL, &status_file_fops);

	return 0;
err:
        unbind_from_irqhandler(tmem_irq, NULL);
	/**/					// ++ platero, start
	if (xen_initial_domain())
        {
	    unbind_from_irqhandler(tmem_stats_irq, NULL);
	}
	/**/					// ++ platero, end
        return ret;
}

module_init(xen_tmem_init);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dan Magenheimer <dan.magenheimer@oracle.com>");
MODULE_DESCRIPTION("Shim to Xen transcendent memory");

