#ifndef _XEN_TMEM_H
#define _XEN_TMEM_H

#include <linux/types.h>

#ifdef CONFIG_XEN_TMEM_MODULE
#define tmem_enabled true
#else
/* defined in drivers/xen/tmem.c */
extern bool tmem_enabled;
#endif

#ifdef CONFIG_XEN_SELFBALLOONING
extern int32_t selfballoon_settotalrampages(uint64_t trp);
extern int xen_selfballoon_init(bool, bool, unsigned int);
#endif

#endif /* _XEN_TMEM_H */
